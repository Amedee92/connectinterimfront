import { ConnectInterimFrontPage } from './app.po';

describe('connect-interim-front App', () => {
  let page: ConnectInterimFrontPage;

  beforeEach(() => {
    page = new ConnectInterimFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
